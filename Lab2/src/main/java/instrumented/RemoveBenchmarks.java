package instrumented;


import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import repository.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class RemoveBenchmarks {
    private static final Collection<Order> orders = new ArrayList<>();

    public RemoveBenchmarks() {
        for (int i = 0; i < 1000; i++) {
            orders.add(new Order(i,i,i));
        }
        Collections.shuffle((List<?>) orders);
    }


    @State(Scope.Benchmark)
    public static class RepoState {

        ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        FastUtilMapBasedRepository<Order> fastUtilMapBasedRepository = new FastUtilMapBasedRepository<>();
        HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
        KolobokeMapBasedRepository<Order> kolobokeMapBasedRepository = new KolobokeMapBasedRepository<>();
        TreeSetBasedRepository<Order> treeSet = new TreeSetBasedRepository<>();
        ConcurrentHashMapBasedRepository<Order> concurrentHashMap = new ConcurrentHashMapBasedRepository<>();
        UnifiedSetBasedRepository<Order> unifiedSetBasedRepository = new UnifiedSetBasedRepository<>();

        @Setup(Level.Invocation)
        public void setup() {
            for (Order item : orders) {
                arrayListBasedRepository.add(item);
                fastUtilMapBasedRepository.add(item);
                hashSetBasedRepository.add(item);
                kolobokeMapBasedRepository.add(item);
                treeSet.add(item);
                concurrentHashMap.add(item);
                unifiedSetBasedRepository.add(item);
            }
        }

    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_arrayList(RepoState state) {
        for (Order order : orders) {
            state.arrayListBasedRepository.remove(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_fastUtilMap(RepoState state) {
        for (Order order : orders) {
            state.fastUtilMapBasedRepository.remove(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_hashSet(RepoState state) {
        for (Order order : orders) {
            state.hashSetBasedRepository.remove(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_kolobokeMap(RepoState state) {
        for (Order order : orders) {
            state.kolobokeMapBasedRepository.remove(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_treeSet(RepoState state) {
        for (Order order : orders) {
            state.treeSet.remove(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_concurrentHashMap(RepoState state) {
        for (Order order : orders) {
            state.concurrentHashMap.remove(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_unifiedSet(RepoState state) {
        for (Order order : orders) {
            state.unifiedSetBasedRepository.remove(order);
        }
    }
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(RemoveBenchmarks.class.getSimpleName())
                //.addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }


}

