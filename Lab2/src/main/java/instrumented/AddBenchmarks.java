package instrumented;


import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import repository.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class AddBenchmarks {
    private static final Collection<Order> orders = new ArrayList<>();

    public AddBenchmarks() {
        for (int i = 0; i < 1000; i++) {
            orders.add(new Order(i,i,i));
        }
        Collections.shuffle((List<?>) orders);
    }


    @State(Scope.Benchmark)
    public static class RepoState {

        ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        FastUtilMapBasedRepository<Order> fastUtilMapBasedRepository = new FastUtilMapBasedRepository<>();
        HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
        KolobokeMapBasedRepository<Order> kolobokeMapBasedRepository = new KolobokeMapBasedRepository<>();
        TreeSetBasedRepository<Order> treeSet = new TreeSetBasedRepository<>();
        ConcurrentHashMapBasedRepository<Order> concurrentHashMap = new ConcurrentHashMapBasedRepository<>();
        UnifiedSetBasedRepository<Order> unifiedSetBasedRepository = new UnifiedSetBasedRepository<>();

        @Setup(Level.Invocation)
        public void setup() {
            for (Order item : orders) {
                arrayListBasedRepository.remove(item);
                fastUtilMapBasedRepository.remove(item);
                hashSetBasedRepository.remove(item);
                kolobokeMapBasedRepository.remove(item);
                treeSet.remove(item);
                concurrentHashMap.remove(item);
                unifiedSetBasedRepository.remove(item);
            }
        }

    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_arrayList(RepoState state) {
        for (Order order : orders) {
            state.arrayListBasedRepository.add(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_fastUtilMap(RepoState state) {
        for (Order order : orders) {
            state.fastUtilMapBasedRepository.add(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_hashSet(RepoState state) {
        for (Order order : orders) {
            state.hashSetBasedRepository.add(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_kolobokeMap(RepoState state) {
        for (Order order : orders) {
            state.kolobokeMapBasedRepository.add(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_treeSet(RepoState state) {
        for (Order order : orders) {
            state.treeSet.add(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_concurrentHashMap(RepoState state) {
        for (Order order : orders) {
            state.concurrentHashMap.add(order);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput})
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
    @Measurement(iterations = 5, time = 2)
    @Fork(value = 1, warmups = 1)
    public void add_unifiedSet(RepoState state) {
        for (Order order : orders) {
            state.unifiedSetBasedRepository.add(order);
        }
    }
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(AddBenchmarks.class.getSimpleName())
                //.addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }


}

