package repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository <T> implements InMemoryRepository<T>{

    private final Set<T> treeSet;

    public TreeSetBasedRepository() {
        this.treeSet = new TreeSet<T>();
    }

    @Override
    public void add(T object) {
        treeSet.add(object);
    }

    @Override
    public void remove(T object) {
        treeSet.remove(object);
    }

    @Override
    public boolean contains(T object) {
        return treeSet.contains(object);
    }
}
