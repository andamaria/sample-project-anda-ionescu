package repository;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Set;

public class UnifiedSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> set;

    public UnifiedSetBasedRepository() {
        this.set = new UnifiedSet<T>();
    }

    @Override
    public void add(T object) {
        this.set.add(object);
    }

    @Override
    public void remove(T object) {
        this.set.remove(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }
}
