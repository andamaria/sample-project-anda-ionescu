package repository;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {

    private final Map<Integer,T> concurrentHashMap;
    private int key;

    public ConcurrentHashMapBasedRepository() {
        this.concurrentHashMap = new ConcurrentHashMap<Integer,T>();
        this.key = 0;
    }

    @Override
    public void add(T object) {
        concurrentHashMap.put(this.key, object);
        this.key += 1;
    }

    @Override
    public void remove(T object) {
        concurrentHashMap.values().remove(object);
    }

    @Override
    public boolean contains(T object) {
        return concurrentHashMap.containsValue(object);
    }
}
