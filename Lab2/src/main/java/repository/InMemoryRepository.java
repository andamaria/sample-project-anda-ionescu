package repository;

public interface InMemoryRepository<T> {

     void add(T object);
     void remove(T object);
     boolean contains(T object);

}
