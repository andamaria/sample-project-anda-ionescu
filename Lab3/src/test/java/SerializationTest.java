import javaFiles.DecimalValueOuterClass;
import junit.framework.TestCase;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

class SerializationTest {

    @org.junit.jupiter.api.Test
    void smallTest() {
        List<BigDecimal> bigDecimalsList = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            bigDecimalsList.add(new BigDecimal(i));
        }

        Serialization serialization = new Serialization(bigDecimalsList);

        List<DecimalValueOuterClass.DecimalValue> serializedElements = serialization.serialize();
        List<java.math.BigDecimal> deserializedElements = serialization.deserialize(serializedElements);
        Assertions.assertEquals(bigDecimalsList,deserializedElements);
    }

    @org.junit.jupiter.api.Test
    void bigTest() {
        List<BigDecimal> bigDecimalsList = new ArrayList<>();
        for (int i = 1; i <= 100000000; i++) {
            bigDecimalsList.add(new BigDecimal(i%10000));
        }

        Serialization serialization = new Serialization(bigDecimalsList);

        List<DecimalValueOuterClass.DecimalValue> serializedElements = serialization.serialize();
        List<java.math.BigDecimal> deserializedElements = serialization.deserialize(serializedElements);
        Assertions.assertEquals(bigDecimalsList,deserializedElements);
    }

}