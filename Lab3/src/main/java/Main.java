import com.google.protobuf.ByteString;
import javaFiles.DecimalValueOuterClass;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<BigDecimal> bigDecimalList = new ArrayList<BigDecimal>();
        for (int i = 0; i < 1000; i++) {
            bigDecimalList.add(new BigDecimal(i));
        }

        for (int i = 0; i < bigDecimalList.size(); i++) {

            DecimalValueOuterClass.DecimalValue serialized = DecimalValueOuterClass.DecimalValue.newBuilder()
                    .setScale(bigDecimalList.get(i).scale())
                    .setPrecision(bigDecimalList.get(i).precision())
                    .setValue(ByteString.copyFrom(bigDecimalList.get(i).unscaledValue().toByteArray()))
                    .build();

            java.math.MathContext mc = new java.math.MathContext(serialized.getPrecision());

            java.math.BigDecimal deserialized = new java.math.BigDecimal(
                    new java.math.BigInteger(serialized.getValue().toByteArray()),
                    serialized.getScale(),
                    mc);

            System.out.println(i + " : " + bigDecimalList.get(i).equals(deserialized));
        }

    }
}
