import com.google.protobuf.ByteString;
import javaFiles.DecimalValueOuterClass;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Serialization {

    private final List<BigDecimal> bigDecimalList;

    public Serialization(List<BigDecimal> bigDecimalList) {
        this.bigDecimalList = bigDecimalList;
    }

    public List<DecimalValueOuterClass.DecimalValue> serialize() {

        List<DecimalValueOuterClass.DecimalValue> serializedElements = new ArrayList<>();

        for (BigDecimal bigDecimal : bigDecimalList) {
            DecimalValueOuterClass.DecimalValue serialized = DecimalValueOuterClass.DecimalValue.newBuilder()
                    .setScale(bigDecimal.scale())
                    .setPrecision(bigDecimal.precision())
                    .setValue(ByteString.copyFrom(bigDecimal.unscaledValue().toByteArray()))
                    .build();
            serializedElements.add(serialized);
        }
        return serializedElements;
    }


    public List<java.math.BigDecimal> deserialize(List<DecimalValueOuterClass.DecimalValue> serializedElements) {

        List<java.math.BigDecimal> deserializedElements = new ArrayList<>();

        for (DecimalValueOuterClass.DecimalValue decimalValue : serializedElements) {
            java.math.MathContext mc = new java.math.MathContext(decimalValue.getPrecision());
            java.math.BigDecimal deserialized = new java.math.BigDecimal(
                    new java.math.BigInteger(decimalValue.getValue().toByteArray()),
                    decimalValue.getScale(),
                    mc);
            deserializedElements.add(deserialized);
        }
        return deserializedElements;
    }
}
